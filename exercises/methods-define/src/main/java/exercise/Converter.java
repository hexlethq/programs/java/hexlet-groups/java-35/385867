package exercise;

class Converter {


    // BEGIN
    public static void main(String[] args) {
        int r = convert(10, "b");

        System.out.println("10 Kb = " + r + " b");
    }

    public static int convert(int bytesIn, String name) {

        int sizeKb = bytesIn / 1024;
        int sizeBytes = bytesIn * 1024;

        if (name.equals("b")) {
            return sizeBytes;
        } else if (name.equals("Kb")) {
            return sizeKb;
        } else {
            return 0;
        }
    }


}


// END

