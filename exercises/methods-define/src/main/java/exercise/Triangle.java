package exercise;

class Triangle {
    // BEGIN
    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }
    public static double getSquare(double sideA, double sideB, double angle) {
        double angleRadiant = (Math.PI * angle) / 180;
        return (sideA * sideB) / 2 * Math.sin(angleRadiant);
    }
    // END
}
