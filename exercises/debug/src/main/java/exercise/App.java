package exercise;

class App {
    // BEGIN
    static String getTypeOfTriangle(int x, int y, int z) {
        if ((x > 0 && y > 0 && z > 0) && !(x + y < z || x + z < y || y + z < x)) {
            // Check for equilateral triangle
            if (x == y && y == z)
                return "Равносторонний";

                // Check for isoceles triangle
            else if (x == y || y == z || z == x)
                return "Равнобедренный";

                // Check for scalene triangle
            else if (x != y || y != z || z != x)
                return "Разносторонний";
        }

        // Check for not a triangle
            return "Треугольник не существует";



    }

    public static void main(String[] args) {
        int x = 5, y = 6, z = 7;
        System.out.println(getTypeOfTriangle(x, y, z));

    }
    // END

}
