package exercise;

class Card {

    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN

        String format = String.format("%" + starsCount + "s", "");

        String masked = format.replace(' ', '*');

        String digits = cardNumber.substring(12);

        System.out.println(masked + digits);

        // END
    }
}
