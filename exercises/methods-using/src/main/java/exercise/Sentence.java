package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        char lastCharacter =sentence.charAt(sentence.length() - 1);
        if (lastCharacter == '!') {

            System.out.println(sentence.toUpperCase());
        } else {
            System.out.println(sentence.toLowerCase());
        }


        // END
    }
}
