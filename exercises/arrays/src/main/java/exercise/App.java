package exercise;

import java.util.Arrays;

class App {
    // BEGIN Создайте публичный статический метод reverse(), который принимает в качестве аргумента массив целых чисел
    // и возвращает новый массив целых чисел с элементами, расположенными в обратном порядке.
    // Если передан пустой массив, метод должен вернуть пустой массив.
    //int[] numbers = {1, 2, -6, 3, 8};
    //int[] result = App.reverse(numbers);
    //c // => [8, 3, -6, 2, 1]

    public static int[] reverse(int[] numbers) {
        int[] resArray = new int[numbers.length];
        if (numbers.length != 0) {
            for (int i = numbers.length - 1; i >= 0; i--) {
                resArray[numbers.length - (i + 1)] = numbers[i];
            }
        }
        System.out.println(Arrays.toString(resArray));
        return resArray;
    }

     //Создайте публичный статический метод mult().
     // Метод принимает в качестве аргумента массив целых чисел и возвращает произведение всех элементов массива
     public static int mult(int[] numbers) {
         int res = 1;
         for (int i = 0; i < numbers.length; i++) {
             res = numbers[i] * res;
         }
         return res;

     }
}
// END


